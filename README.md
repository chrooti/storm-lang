# Storm Lang (WIP)

## Wanted feature, non-exhaustive list

- Function return jumps (without double checking)
- Implicit constexpr can be "annotated" to emit errors if not evaluated
- Modules
- asm inline
- for/else, defer
- rust like macros but not quite

## Questions

- How do I express the type of a generic pointer (e.g. for casting) in Zig?