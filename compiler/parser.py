from abc import ABC, abstractmethod
from enum import Enum
import re
from typing import Union

# TextState

class SyntaxError(Exception):
    pass

class TextState:
    """
    String on steroids with utility functions for parsing. It should be noted that this class forms the
    API between the entities in the parser module (aka ASTNodes, Parser and SyntaxErrors)
    """

    space_eater = re.compile(r"[ \t\r\f\n]+", re.MULTILINE)

    __slots__ = (
        'text',
        'pos',
        'line_begin_pos',
        'line',
        'line_pos'
    )

    def __init__(
        self, *,
        text: str,
        pos: int,
        line_begin_pos: int,
        line: int,
        line_pos: int
    ):
        # the class doesn't own the string object
        self.text = text

        # current position in the text
        self.pos = pos

        # position where the current line begins
        self.line_begin_pos = line_begin_pos

        # current line index
        self.line = line

        # position relative to the line
        self.line_pos = line_pos

    # CCs (Commnodity Constructors)

    @classmethod
    def new(cls, text: str):
        _new = cls(
            text=text,
            pos=0,
            line_begin_pos=0,
            line=1,
            line_pos=1
        )

        _new.eat_spaces()
        _new.eat_comments()

        return _new

    def copy(self):
        return self.__class__(
            text=self.text,
            pos=self.pos,
            line_begin_pos=self.line_begin_pos,
            line=self.line,
            line_pos=self.line_pos
        )

    # Whitespace helpers

    def advance(self, count):
        self.pos += count
        self.line_pos += count

    def eat_spaces(self):
        match = self.space_eater.match(self.text, self.pos)
        if match:
            full_match = match[0]

            # number of chars to skip
            full_match_len = len(full_match)

            # number of newlines (and therefore number of rows to skip)
            newline_count = full_match.count("\n")

            self.pos += full_match_len
            if newline_count >= 1:
                # relative to the matched string, yeah this is needlessly convolute
                last_newline_relative_pos = full_match.rfind("\n")

                self.line_begin_pos = self.pos - last_newline_relative_pos

                self.line += newline_count
                self.line_pos = full_match_len - last_newline_relative_pos
            else:
                self.line_pos += full_match_len

    # Comment helpers

    def eat_comments(self):
        # TODO: don't hardcode it
        while self.text.startswith("#", self.pos):
            self.pos = self.text.find("\n", self.pos) + 1
            self.line_begin_pos = self.pos

            self.line += 1
            self.line_pos = 1

            self.eat_spaces()

    # Token matching helpers

    def match_string(self, string):
        if not self.text.startswith(string, self.pos):
            return False

        self.advance(len(string))
        self.eat_spaces()
        self.eat_comments()

        return True

    def match_regex(self, regex):
        if not (match := regex.match(self.text, self.pos)):
            return False

        self.advance(match.end() - match.start())
        self.eat_spaces()
        self.eat_comments()

        return match[0]

    # SyntaxError helpers

    def to_syntax_error(self, other_infos: str):
        first_newline = self.text.find("\n", self.pos)
        line_str = f"{self.line}:"
        return SyntaxError(
            f"Parser error:\n"
            f"{line_str} {self.text[self.line_begin_pos:first_newline]}\n"
            f"{'-' * (self.line_pos + len(line_str))}^\n"
            f"{other_infos}\n"
        )


# AST Nodes

class ASTNode:
    tag = None

    __slots__ = (
        'uuid',
        'text_states'
    )

    def __init__(self):
        self.text_states = []

    def set_uuid(self, uuid: str):
        self.uuid = uuid
        
    def add_text_state(self, text_state: TextState):
        self.text_states.append(text_states)

    def __repr__(self):
        return f"!{self.uuid}"

    @classmethod
    def calculate_uuid(cls, *args, **kwargs):
        # this takes at most tag and constructor arguments since it's used exactly to avoid constructing
        # an instance
        return "-".join(
            (cls.tag, *(repr(x) for x in args), *(repr(x) for x in kwargs.values()))
        )

# nodes

# nested rvalues?
# closures are very similar to struct initialization: how to disambiguate?
# "stringhe"
# sintass  init struct: ci serve?
# TODO: struct.cose (forse nested rvalue?)

RValueToken = Union[
    str,  # identifier
    int,  # number
    "ClosureASTNode",
    "StructASTNode",
    "SignatureCallASTNode",
    "RValueASTNode",
]

Statement = Union[
    str,
    "AssignmentASTNode",
    "DefinitionASTNode",
    "SignatureCallASTNode",
]


class AssignmentASTNode(ASTNode):
    # A = B

    tag = "ASSIGNMENT"

    __slots__ = (
        'identifier',
        'rvalue'
    )

    def __init__(self, *, identifier: str, rvalue: "RValueASTNode"):
        super().__init__()
        self.identifier = identifier
        self.rvalue = rvalue

    def __str__(self):
        return f"{self.identifier} = {str(self.rvalue)}"


class DeclarationASTNode(ASTNode):
    # A: B

    tag = "DECLARATION"

    __slots__ = (
        'identifier',
        '_type'
    )

    def __init__(self, *, identifier: str, _type: "RValueASTNode"):
        super().__init__()
        self.identifier = identifier
        self._type = _type

    def __str__(self):
        return f"{self.identifier}: {str(self._type)}"


class DefinitionASTNode(ASTNode):
    # var A (: B) = C

    tag = "DEFINITION"

    __slots__ = (
        'identifier',
        '_type',
        'rvalue'
    )

    def __init__(
        self,
        *,
        identifier: str,
        _type: Union["RValueASTNode", None],
        rvalue: Union["RValueASTNode", None],
    ):
        super().__init__()
        self.identifier = identifier
        self._type = _type
        self.rvalue = rvalue

    def __str__(self):
        if self._type is not None:
            return f"var {self.identifier}: {str(self._type)} = {str(self.rvalue)}"
        else:
            return f"var {self.identifier} = {str(self.rvalue)}"


class ClosureASTNode(ASTNode):
    tag = "CLOSURE"

    __slots__ = (
        'statements'
    )

    def __init__(self, *, statements=list[Statement]):
        self.statements = statements

    def __str__(self):
        statements = "\n".join(str(s) for s in statements)

        return f"{{\n{statements}\n}}"


class StructASTNode(ASTNode):
    tag = "STRUCT"

    __slots__ = (
        'parameters',
        'members'
    )

    def __init__(
        self, *, parameters: list[DeclarationASTNode], members: list[DeclarationASTNode]
    ):
        super().__init__()

        # difference: parameters are part of the type signature, members aren't
        self.parameters = parameters
        self.members = members

    def __str__(self):
        # TODO indent
        parameters = ", ".join(str(x) for x in self.parameters)
        members = "\n".join(str(x) for x in self.members)  # should apply tab
        return f"struct({parameters}) {{\n{members}\n}}"


class FuncASTNode(ASTNode):
    tag = "FUNC"

    __slots__ = (
        'parameters',
        'return_type'
    )

    def __init__(self, *, parameters, return_type):
        super().__init__()
        self.parameters = parameters
        self.return_type = return_type

    def __str__(self):
        parameters = ", ".join(str(x) for x in self.parameters)

        if self.return_type is not None:
            return f"func({parameters}) -> {self.return_type}"
        else:
            return f"func({parameters})"


class SignatureCallASTNode(ASTNode):
    """
    Can either be a function call or our equivalent of generic instantiation.
    Example: f()() --->
    """

    tag = "SIGNATURE_CALL"

    __slots__ = (
        'identifier',
        'calls'
    )

    def __init__(self, *, identifier: str, calls: list[list["RValueASTNode"]]):
        super().__init__()
        self.identifier = identifier
        self.calls = calls

    def __str__(self):
        calls = ")(".join(", ".join(str(x) for x in call) for call in self.calls)

        return f"{self.identifier}({calls})"


class RValueASTNode(ASTNode):
    tag = "RVALUE"

    __slots__ = (
        'children'
    )

    def __init__(self, *, children: list[RValueToken]):
        super().__init__()
        self.children = children

    def __str__(self):
        return " ".join(
            f"({x})" if isinstance(x, RValueASTNode) else str(x) for x in self.children
        )


class ASTNodeType(Enum):
    # declaration: declares a variable
    # definition: lays out how e.g. a struct is made
    # e.g. a declaration can be something like "identifier = struct_definition"

    ASSIGNMENT = AssignmentASTNode
    DECLARATION = DeclarationASTNode
    DEFINITION = DefinitionASTNode
    CLOSURE = ClosureASTNode
    STRUCT = StructASTNode
    FUNC = FuncASTNode
    SIGNATURE_CALL = SignatureCallASTNode
    RVALUE = RValueASTNode


class ASTNodeRegistry:
    __slots__ = ('nodes')

    def __init__(self):
        self.nodes = {}

    def get(self, node_type: ASTNodeType, *args, **kwargs):
        # calculate_uuid must have the same signature as the constructor
        node_uuid = node_type.value.calculate_uuid(*args, **kwargs)
        node = self.nodes.get(node_uuid)
        if not node:
            # construct the node with kwargs as arguments
            node = node_type.value(*args, **kwargs)
            node.set_uuid(node_uuid)
            # node.check() TODO
            self.nodes[node_uuid] = node

        return node


class AST:
    __slots__ = ('children')

    def __init__(self):
        self.children = []

    def add_child(self, child):
        self.children.append(child)

    def __getitem__(self, idx: int):
        return self.children[idx]

    def __str__(self):
        return "\n\n".join(str(x) for x in self.children)


# Actual parser

class Parser:

    identifier = re.compile(r"[A-Za-z_@][A-Za-z0-9_]*")
    number = re.compile(r"[0-9']+")


    __slots__ = (
        'text',
        'text_state',
        'keywords',
        'continuators',
        'result',
        'token_label',
        'node_registry',
        'ast',
        'nesting_level'
    )

    def __init__(self, text: str):
        self.text_state = TextState.new(text)


        # fmt: off
        self.keywords = set((
            # types
            "struct",
            "func",
            "enum",

            # control flow
            "if",
            "else"
            "while",

            # modules
            "module",
            "import",
            "as",

            # other keywords
            "var",
        ))
        self.continuators = [
            # arithmetic ops
            "+", "-", "*", "/", "%",

            # logical
            "!", "&&", "||",

            # bitwise
            "~", "&", "|", "^", ">>", "<<",

            # comparison ops
            "==", "!=", "<=", ">=", ">", "<",">", "<",
        ]
        # fmt: on
        self.continuators.sort(key=lambda x: len(x), reverse=True)

        self.result = None
        self.token_label = ''

        self.node_registry = ASTNodeRegistry()
        self.ast = AST()
        self.nesting_level = 0

    # matching helpers

    def parser(*, label=None):
        def wrapper(func):
            if label is None:
                def wrapped(self, *args, **kwargs):
                    self.result = func(self, *args, **kwargs)
                    return self
            else:
                def wrapped(self, *args, **kwargs):
                    self.result = func(self, *args, **kwargs)
                    self.token_label = label
                    return self

            return wrapped
        return wrapper

    def unwrap(self):
        return self.result

    def expect(self):
        result = self.result
        if result is False:
            raise self.text_state.to_syntax_error("How?")

        return result

    def expect_not_in_keywords(self, identifier):
        if identifier in self.keywords:
            # TODO: report the position correctly before, not after
            raise self.text_state.to_syntax_error(f"Reserved keyword \"{identifier}\" used as identifier")

    @parser()
    def parse_string(self, string):
        return self.text_state.match_string(string)

    @parser()
    def parse_regex(self, regex):
        return self.text_state.match_regex(regex)

    # actual parsers

    @parser(label="identifier")
    def parse_identifier(self):
        return self.parse_regex(self.identifier).unwrap()

    @parser(label="declaration")
    def parse_declaration(self):
        if not (identifier := self.parse_identifier().expect()):
            return False

        self.expect_not_in_keywords(identifier)

        self.parse_string(":").expect()
        _type = self.parse_rvalue().expect()

        return self.node_registry.get(
            ASTNodeType.DECLARATION, identifier=identifier, _type=_type
        )

    @parser(label="signature")
    def parse_signature(self):
        # TODO: make it none in caller
        if not self.parse_string("(").unwrap():
            return False

        signature = []
        while not self.parse_string(")").unwrap():
            declaration = self.parse_declaration().expect()
            signature.append(declaration)
            self.parse_string(",")

        return signature

    @parser(label="signature call")
    def parse_signature_call_parameters(self):
        if not self.parse_string("(").unwrap():
            return False

        # modify above too
        parameters = []
        while not self.parse_string(")").unwrap():
            parameter = self.parse_rvalue().expect()
            parameters.append(parameter)
            self.parse_string(",")

        return parameters

    @parser(label="rvalue token")
    def parse_rvalue_token(self):
        if self.parse_string("{").unwrap():
            # closure
            statements = []
            while not self.parse_string("}").unwrap():
                statement = self.parse_statement().expect()
                statements.append(statement)

            return self.node_registry.get(ASTNodeType.CLOSURE, statements=statements)

        if identifier := self.parse_identifier().unwrap():

            if identifier == "struct":
                # struct type

                parameters = self.parse_signature().unwrap()
                self.parse_string("{").expect()

                members = []
                while not self.parse_string("}").unwrap():
                    member = self.parse_declaration().expect()
                    members.append(member)

                return self.node_registry.get(
                    ASTNodeType.STRUCT, parameters=parameters, members=members
                )

            if identifier == "func":
                # function type
                parameters = self.parse_signature().expect()
                return_type = (
                    self.parse_rvalue().expect()
                    if self.parse_string("->").unwrap()
                    else None
                )

                return self.node_registry.get(
                    ASTNodeType.FUNC, parameters=parameters, return_type=return_type
                )

            self.expect_not_in_keywords(identifier)
            
            if (
                parameters := self.parse_signature_call_parameters().unwrap()
            ) is not False:
                # signature call
                calls = [parameters]
                while (
                    parameters := self.parse_signature_call_parameters().unwrap()
                ) is not False:
                    calls.append(parameters)

                return self.node_registry.get(
                    ASTNodeType.SIGNATURE_CALL, identifier=identifier, calls=calls
                )
            
            return identifier

        if number := self.parse_regex(self.number).unwrap():
            return number

        raise SyntaxError(self, "Expected rvalue")

    @parser(label="continuator")
    def parse_continuator(self):
        for continuator in self.continuators:
            # TODO: small optimization "parse_oneof"
            if self.parse_string(continuator).unwrap():
                return continuator
        return False

    @parser()
    def parse_rvalue_or_rvalue_token(self):
        if self.parse_string("(").unwrap():
            rvalue = self.parse_rvalue().expect()
            self.parse_string(")").expect()
        else:
            rvalue = self.parse_rvalue_token().expect()

        return rvalue

    @parser(label="rvalue")
    def parse_rvalue(self):
        children = [self.parse_rvalue_or_rvalue_token().expect()]

        while continuator := self.parse_continuator().unwrap():
            children.append(continuator)
            children.append(self.parse_rvalue_or_rvalue_token().expect())

        return self.node_registry.get(ASTNodeType.RVALUE, children=children)

    @parser(label="statement")
    def parse_statement(self):
        if identifier := self.parse_identifier().unwrap():
            if identifier == "var":
                # defintion
                identifier = self.parse_identifier().expect()
                self.expect_not_in_keywords(identifier)

                _type = (
                    self.parse_rvalue().expect()
                    if self.parse_string(":").unwrap()
                    else None
                )
                rvalue = (
                    self.parse_rvalue().expect()
                    if self.parse_string("=").unwrap()
                    else None
                )

                return self.node_registry.get(
                    ASTNodeType.DEFINITION,
                    identifier=identifier,
                    _type=_type,
                    rvalue=rvalue,
                )

            self.expect_not_in_keywords(identifier)

            if self.parse_string("=").unwrap():
                # assignment
                rvalue = self.parse_rvalue().expect()
                return self.node_registry.get(
                    ASTNodeType.ASSIGNMENT, identifier=identifier, rvalue=rvalue
                )

            if (
                parameters := self.parse_signature_call_parameters().unwrap()
            ) is not False:
                # signature call that is always a functin call
                calls = [parameters]
                while (
                    parameters := self.parse_signature_call_parameters().unwrap()
                ) is not False:
                    calls.append(parameters)

                return self.node_registry.get(
                    ASTNodeType.SIGNATURE_CALL, identifier=identifier, calls=calls
                )

        raise SyntaxError(self, "Expected statement")

    def parse(self):
        while True:
            node = self.parse_statement().expect()
            self.ast.add_child(node)
        return self.ast
