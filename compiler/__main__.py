from .parser import Parser, SyntaxError

with open("example.storm", "r") as f:
    text = f.read()

parser = Parser(text)
try:
    ast = parser.parse()
    print(ast)
except SyntaxError as e:
    print(parser.ast)
    print("\n")
    print(e)
